'''
Created on Apr 28, 2014

@author: Jay Barra
'''
import sys

from PyQt5.QtWidgets import QApplication, QDialog
from auzio import auzio

def main():
  app = QApplication(sys.argv)

  az = None
  try:
    az = auzio()
  except Exception as e:
    print(e)
  
  sys.exit(app.exec_())
    
if __name__ == "__main__":
    main()
