'''
Created on Apr 28, 2014

@author: jay
'''

from PyQt5.QtWidgets import QMainWindow, QWidget, QDesktopWidget, \
                            QAction, QFileDialog, qApp
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QCoreApplication

import wave
import matplotlib

class auzio(QMainWindow):
  
  def __init__(self):
    super(auzio, self).__init__()
    self.initUi()

  def initUi(self):
    central = QWidget()
    self.setCentralWidget(central)
    
    self.createMenubar()
    
    self.setWindowTitle("Auzio")
    self.resize(800, 600)
    self.move(QDesktopWidget().availableGeometry().center() - self.frameGeometry().center())
    self.show()
    
  def createMenubar(self):
    menubar = self.menuBar()
     
    #Create the File Menu
    qOpenAction = QAction(QIcon('auzio/media/open.png'), '&Open', self)
    qOpenAction.setShortcut('Ctrl+O')
    qOpenAction.setStatusTip('Open a file')
    qOpenAction.triggered.connect(self.openFile)
      
    qCloseAction = QAction('&Close', self)
    qCloseAction.setShortcut('Ctrl+X')
    qCloseAction.setStatusTip('Close the current file')    
        
    qExitAction = QAction(QIcon('auzio/media/Crystal_Project_Exit.png'), '&Exit', self)        
    qExitAction.setShortcut('Ctrl+Q')
    qExitAction.setStatusTip('Exit application')
    qExitAction.triggered.connect(qApp.quit)
        
    qFileMenu = menubar.addMenu('&File')
    qFileMenu.addAction(qOpenAction)
    qFileMenu.addAction(qCloseAction)
    qFileMenu.addSeparator()
    qFileMenu.addAction(qExitAction)
        
    # Create the Stream Menu
    qConnectToStreamAction = QAction(QIcon('auzio/media/connect.png'), '&Connect to Stream', self)        
    qConnectToStreamAction.setShortcut('Ctrl+C')
    qConnectToStreamAction.setStatusTip('Connect to Stream')
        
    qDisonnectFromStreamAction = QAction(QIcon('auzio/media/disconnect.png'), '&Disconnect from Stream', self)        
    qDisonnectFromStreamAction.setShortcut('Ctrl+D')
    qDisonnectFromStreamAction.setStatusTip('&Disconnect from Stream')
    qDisonnectFromStreamAction.setEnabled(False)
        
    qStreamMenu = menubar.addMenu('&Stream')
    qStreamMenu.addAction(qConnectToStreamAction)
    qStreamMenu.addAction(qDisonnectFromStreamAction)
     
    menubar.addMenu('&Help')
 
  def openFile(self):
    try:
      fname = QFileDialog.getOpenFileName(self, 'Open file', '/home/jay/Desktop', "Wave files (*.wav)")
      if fname:
        print(fname)
        fh = wave.open(fname[0], 'r')
        print(fh.getnchannels(), fh.getnframes(), fh.getsampwidth(), fh.getframerate())
    except:
      raise
    