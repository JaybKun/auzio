'''
Created on Mar 26, 2014

@author: jay
'''

#import pylab
#from scipy import pi, sin

# import matplotlib.pyplot as plt
# from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
# from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
# 
#   
# class Visualizer(QtGui.QWidget):
#   '''
#   Base class for displaying matplotlib figures
#   ''' 
#   def __init__(self):
#     super(Visualizer, self).__init__()
#     
#     self.layout = QtGui.QVBoxLayout()
#     self.setLayout(self.layout)
#   
#     self.figure = plt.figure()
#     self.canvas = FigureCanvas(self.figure)
#     
#     self.toolbar = NavigationToolbar(self.canvas, self)
# 
#     self.layout.addWidget(self.canvas)
#     self.layout.addWidget(self.toolbar)
#     
#     self.canvas.draw()
#      
#   def plot(self, signal, samplerate):
#     pass
# 
# 
# class Spectrogram(Visualizer):
#   '''
#   Spectrogram Visualizer
#   '''
# 
#   def __init__(self):
#     super(Spectrogram, self).__init__()
#     
#   def plot(self, signal, samplerate):
#     ax = self.figure.add_subplot(111)
#     ax.hold(False)
#     t = pylab.arange(0, signal.__len__(), 1)
#     try:
#       ax.plot(t, signal)
#       pylab.specgram(signal, NFFT = 1024, Fs = samplerate)
#     except:
#       raise
#     self.canvas.draw()
#     
# class Waveform(Visualizer):
#   '''
#   Waveform Visualizer
#   '''
#   
#   def __init__(self):
#     super(Waveform, self).__init__()
# 
#   def plot(self, signal, samplerate):
#     ax = self.figure.add_subplot(111)
#     ax.hold(False)
#     t = pylab.arange(0.0, signal.__len__(), 1)
#     try:
#       ax.plot(t, signal)
#     except:
#       raise
#     self.canvas.draw()
