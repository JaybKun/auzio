'''
Created on Apr 23, 2014

@author: jay
'''

from threading import Lock

class CircularBuffer(object):
    '''
    classdocs
    '''

    def __init__(self, samplerate = 8000, time = 30):
        '''
        Constructor
        '''
        self._buffer = []
        self._offset = 0
        self._time = time
        
        self._lock = Lock()
        
        for _ in range(time):
          self._buffer.append([0.0] * samplerate)
          
    def _incrementOffset(self):
      self._offset += 1
      self._offset = self._offset % self._time
      
    def insertFrame(self, frame):
      self._lock.acquire()
      self._buffer[self._offset] = frame
      self._incrementOffset()
      self._lock.release()
      
    def buffer(self):
      data = []
      self._lock.acquire()
      try:
        for frame in self._time:
          offset = (self._offset + frame) % self._time
          data[frame] = self._buffer[offset]
      finally:
        self._lock.release()
        
      return data
      
      