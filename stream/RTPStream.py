'''
Created on Apr 28, 2014

@author: jay
'''

import socket
import struct
from threading import Thread

'''
class Stream(Thread):
  def __init__(self, address):
    super(Stream, self).__init__()
    
    self._address = address
    
  def run(self):
    
    try:
      
    finally:
    
'''

def RTPStreamIn(): 
  UDP_IP = "127.0.0.1"
  UDP_PORT = 5004
  SIZE = 64
   
  sock = socket.socket(socket.AF_INET, # Internet
                       socket.SOCK_DGRAM) # UDP
  sock.bind((UDP_IP, UDP_PORT))
    
  while True:
    
    raw_data = []
    try:
      raw_data = sock.recv(64) # buffer size is 1024 bytes
      
    except Exception as e:
      print e
    
    data = struct.unpack(raw_data, 'f')
    print data 