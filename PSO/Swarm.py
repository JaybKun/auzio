'''
Created on Mar 29, 2014

@author: jay
'''
from PSO import Particle

class Swarm(object):
  def __init__(self, size = 1, dims = 1):
    
    self._size = size
    self._dims = dims
    
    self._swarm = []
    
    for _ in xrange(size):
      self._swarm.append(Particle.Particle(dims))
        
    self._best = [None] * dims
    self._pos = [None] * dims
    
  def reset(self):
    del self._best
    del self._pos
    self._best = [None] * self._dims
    self._pos = [None] * self._dims
      
  def best(self):
    return self._pos, self._best
  
  def start(self):
    for particle in self._swarm:
      print(particle)
      particle.start()
  
  def stop(self):
    for particle in self._swarm:
      particle.join() 
