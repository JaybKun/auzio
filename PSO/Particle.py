'''
Created on Mar 29, 2014

@author: jay
'''
from threading import Thread

class Particle(Thread):
  def __init__(self, dims):
    super(Particle, self).__init__()
  
    try:
      if dims <= 1:
        dims = 1
    except:
      dims = 1
      
    self._current = ([None] * dims, [None] * dims)
    self._best = ([None] * dims, [None] * dims)
    self._stopRequest = False
    
  def run(self):
    while self._stopRequest is not True: 
      pass
  
  def join(self, timeout=None):
    self._stopRequest = True
    super(Particle, self).join(timeout)

  